package testpackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;

import common_methods.API_Trigger;
import common_methods.Utility;
import Repository.Req_Body;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_2 extends Req_Body {

	public static void executor() throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");

		String requestBody = Req_Body("Post_TC1");
		String Endpoint = Req_Body.Hostname() + Req_Body.Resource();
		Response response = API_Trigger.Post_trigger(Req_Body.HeaderName(), Req_Body.HeaderValue(),
				requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}

	private static String Req_Body(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}

