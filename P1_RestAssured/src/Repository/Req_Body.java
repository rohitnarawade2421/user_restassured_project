package Repository;

import java.io.IOException;
import java.util.ArrayList;
import common_methods.Utility;

public class Req_Body extends Environment {

	public static String createBody(String TestCase) throws IOException {
		ArrayList<String> data = Utility.readExcelData("Post_API", TestCase);
		String key_firstName = data.get(1);
		String value_firstName = data.get(2);
		String key_lastName = data.get(3);
		String value_lastName = data.get(4);
		String key_age = data.get(5);
		String value_age = data.get(6);
		String req_body = "{\r\n" + "    \"" + key_firstName + "\": \"" + value_firstName + "\",\r\n" + "    \""
				+ key_lastName + "\": \"" + value_lastName + "\",\r\n" + "    \"" + key_age + "\": \"" + value_age
				+ "\"\r\n" + "}";
		return req_body;
	}

	public static String updateBody(String TestCase) throws IOException {
		ArrayList<String> data = Utility.readExcelData("Put_API", TestCase);
		String key_lastName = data.get(3);
		String value_lastName = data.get(4);
		String req_body = "{\r\n" + "    \"" + key_lastName + "\": \"" + value_lastName + "\"\r\n" + "}";
		return req_body;

	}
}
