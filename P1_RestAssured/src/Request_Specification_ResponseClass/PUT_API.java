package Request_Specification_ResponseClass;
import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PUT_API {

	public static void main(String[] args) {
		// Store all required values  in variables
		
					String req_body = "{\n"
							+ "    \"name\": \"morpheus\",\n"
							+ "    \"job\": \"zion resident\"\n"
							+ "}";
					String hostname = "		https://reqres.in";
					String resource = "/api/users/2";
					String headername = "Content-Type";
					String headervalue = "application/json";	

					// Build Request Specification
					
					RequestSpecification req_spec = RestAssured.given();
					
					req_spec.header(headername,headervalue);
					
					req_spec.body(req_body);
					
					Response response = req_spec.put(hostname + resource);
					
					System.out.println(response.getBody().asString());
					System.out.println(response.statusCode());
					
					
					// create jsonpath object for parsing
					
					JsonPath j_req = new JsonPath(req_body);
					
					
					// get current date
					
					LocalDateTime  currentDate = LocalDateTime.now();
			        String expectedDate = currentDate.toString().substring(0, 11);
			        
					
					//Using TestNG for validation
					
					Assert.assertEquals(j_req.getString("name"),response.jsonPath().getString("name"));
					
					Assert.assertEquals(j_req.getString("job"),response.jsonPath().getString("job"));
					
					Assert.assertEquals(expectedDate,response.jsonPath().getString("updatedAt").substring(0,11));

	}

}
