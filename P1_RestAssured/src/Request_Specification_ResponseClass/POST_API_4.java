package Request_Specification_ResponseClass;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class POST_API_4 {
	public static void main(String[] args) {

		// Store all required values  in variables
		
		String req_body = "{\n"
				+ "    \"email\": \"eve.holt@reqres.in\",\n"
				+ "    \"password\": \"cityslicka\"\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/login";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
		// Build request Specification
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername , headervalue);
		
		req_spec.body(req_body);
		
		Response response = req_spec.post(hostname + resource);
		
		System.out.println(response.getBody().asString());
		System.out.println(response.statusCode());
		
		
		// using TestNG
		
		Assert.assertNotNull(response.getBody().jsonPath().getString("token"));
	}

}
