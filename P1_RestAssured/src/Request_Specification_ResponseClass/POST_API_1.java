package Request_Specification_ResponseClass;
import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class POST_API_1 {

	public static void main(String[] args) {
		//step 1 : collect all required info and save into local variable
		
		String req_body = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
		
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
// Step 2 : build the request specification by using request specification class
	
	RequestSpecification req_spec = RestAssured.given();
		
// step 2.1 set request header
		
	req_spec.header(headername, headervalue);
	
// step 2.2	set the request body8
	
	req_spec.body(req_body);
	
// step 2.3 trigger the api request
	
Response response = req_spec.post(hostname + resource);
	
	
System.out.println(response.getBody().asString());
		
System.out.println(response.statusCode());


String createdAt = response.getBody().jsonPath().getString("createdAt").substring(0,11);
System.out.println(createdAt);

//Fetch Request body

JsonPath j_req = new JsonPath(req_body);

String req_name = j_req.getString("name");
System.out.println(req_name);
String req_job = j_req.getString("job");
System.out.println(req_job);
//create expected date

LocalDateTime  currentDate = LocalDateTime.now();
String expectedDate = currentDate.toString().substring(0, 11);

// Using test NG

Assert.assertEquals(response.getBody().jsonPath().getString("name"), j_req.getString("name"));
Assert.assertEquals(response.getBody().jsonPath().getString("job"), j_req.getString("job"));
Assert.assertNotNull(response.getBody().jsonPath().getString("id"));
Assert.assertEquals(createdAt, expectedDate);

	}

}
