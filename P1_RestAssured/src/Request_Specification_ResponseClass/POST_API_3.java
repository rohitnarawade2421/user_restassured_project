package Request_Specification_ResponseClass;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class POST_API_3 {

	public static void main(String[] args) {
		// Step 1 : collect all variables
		
		String req_body = "{\n"
				+ "    \"email\": \"sydney@fife\"\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/register";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
// Build request Specification class
		
	RequestSpecification req_spec = RestAssured.given();
	
	req_spec.header(headername,headervalue);
	
	req_spec.body(req_body);
	
	Response response = req_spec.post(hostname + resource);
	
	System.out.println(response.getBody().asString());
	
	System.out.println(response.statusCode());

	}

}
