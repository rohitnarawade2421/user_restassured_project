package APIreference;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;

public class ReastPostAPI404 {

	public static void main(String[] args) {
		// collect all variables 
		
				String req_body = "{\n"
						+ "    \"email\": \"sydney@fife\"\n"
						+ "}" ;
				
				String hostname = "https://reqres.in";
				String resource = "/api/register";
				String headername = "Content-Type";
				String headervalue = "application/json";
				// declare base URI
				
				RestAssured.baseURI = hostname;
				
				//configure API
				
				String res_body= given().header(headername,headervalue).body(req_body)
				.when().post(resource)
				.then().extract().response().asString();
				
				System.out.println(res_body);
				
				//configure API with status code
				
				int status_code= given().header(headername,headervalue).body(req_body)
						.when().post(resource)
						.then().extract().statusCode();
						
						System.out.println(status_code);

	}

}
